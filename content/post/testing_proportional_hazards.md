+++
date = "2016-05-01T10:25:50+01:00"
title = "Testing Proportional Hazards"

+++

In a previous [post](/post/cox_proportional_hazard) we covered the assumptions of the Cox Proportional Hazards model.  The model places very little restriction on the shape of the baseline hazard function or on the linear predictor, however, it does assume that the linear predictor has a constant proportional effect on the hazard function.

<div>$$\frac{h(t|x_0)}{h(t|x_1)} = \frac{h_0(t)e^{x_0\beta}} {h_0(t)e^{x_1\beta}} = e^{(x_0 - x_1)\beta}$$

This assumption is definitely non-trivial, and can be prone to fail.  An interesting thing to note is that this assumptions can fail even if every member of the population actually meets proportional hazard individually.  For example, if there is heterogenity in the population (some people have shorter lives than others), then through time we will be left with the longer lived members of the population, so the measured effect of beta will change through time.

The most intuitive way to test the assumption is to just include time varying covariates in the regressions and see whether they have siginificant coefficients.  If they do, then there is time varying information that we are not capturing and hence the assumption is false.

Unfortunately, there isn't an easy way to do this in python, as lifelines doesn't offer time varying covariates.  

More information on how to test the assumptions can be found at the following links:

https://www.researchgate.net/profile/Richard_Gill3/publication/31086716_A_simple_test_of_the_proportional_hazards_assumption/links/00b7d52bc3a4063d0f000000.pdf

http://www.econ.hit-u.ac.jp/~tshiba/Schoenfeld1982.pdf

http://www.ats.ucla.edu/stat/examples/asa/test_proportionality.htm

