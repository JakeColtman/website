+++
date = "2016-05-06T10:30:21+01:00"
title = "PyData 2016 - London"
description = "All the information needed to accompany the presentation"
+++

The presentation will be done through the medium of Jupyter notebooks.  Below are different notebooks for varying staging of completeness.  I recommend trying to complete everything yourself as we go along, but if you get stuck or lost then you can jump to a more completed work book.

#### Data

[Download here](/pyData/output.txt)

Designed to be roughly similar to DCM path to conversion data.  Each user has an id and between one and two rows.  The first row in time will always be when a user saw an advert.  If a user converts then they have a second row for conversion time.  Users who never convert will have only one row.


#### Workbooks

[Base workbook - start here](/pyData/Basic Presentation.ipynb)


[Data Cleaning Done](/pyData/Data Mungining Done.ipynb)


[pymc 1](/pyData/PyMC Part 1 Done.ipynb)

[pymc 2](/pyData/PyMC Part 2 Done.ipynb)

[pymc 3](/pyData/PyMC Done.ipynb)


[Cox Done](/pyData/Cox Done.ipynb)
[Full Version](/pyData/Full done.ipynb)

#### Libraries used:

We will use three non-standard libraries, pymc a Bayesian library, lifelines a library for survival analysis and pyBMA a library for Bayesian model averaging.  

[lifelines](http://lifelines.readthedocs.io/en/latest/Survival%20Regression.html#cox-s-proportional-hazard-model) - pip install lifelines

[pymc](https://pymc-devs.github.io/pymc/) - pip install pymc

[pyBMA] (http://google.com) - pip install pyBMA

#### After the talk

I've tried to provide fuller information on everything we go through in the presentation on this site, so if you would like to learn more about anything, please read through.  There are also links to relevant academic papers if you wish to really dig in.

Failing that, if you have any questions or issues please feel free to contact me at jakecoltman @ sky.com
