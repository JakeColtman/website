+++
date = "2016-05-04T09:18:25+01:00"
draft = true
title = "MCMC Checking"
description = "Guidelines on how to check your MCMC"
+++

The core concept of MCMC is that we can sample parameter space to gain increasingly good approximations for the distribution we're interested in.  There are two potential problems with the process that we need to check when we run MCMC.

#### Convergence

No matter what starting values of parameters we use, we want MCMC to converge to a steady distribution.  Given that the starting values may by in an area of low probability, the first M iterations of the algorithm will be dependent upon starting point and not approximate the distribution well.  Unfortunately, there is no easy way to tell in advance how large M will be.

While there are a number of tests to determine whether the algorithm has converged (see Gelman and Rubin 1992, Geweke
 1992), for our purposes it is sufficient to do graphical tests.  The simplest way to do this is to use pymc's inbuilt plotting program:
 
 {{< highlight python >}}
pymc.Matplot.plot(mcmc)
 {{< /highlight >}}
 
 This will produce images that look like these:
 
##### Good
![Good](/good.PNG)

##### Bad
![Bad](/bad.PNG)

Note how in the first image the top left diagram has a consistent distribution, while in the lower image the early samples are markedly different.

To improve our estimates, we want to ignore the opening part of the trace where we haven't yet converged.  This can be done in one of two ways.  We can specify a "burn" parameter at modelling fitting time which ignored the first N samples, or we can just directly slice the trace to exclude them.  While the former is code cleaner, the latter allows you to play with the parameter more easily.

  {{< highlight python >}}
burn_number = 1000
mcmc = pm.MCMC(model)
	
##Either
mcmc.sample(50000, burn = burn_number)
	
##Or
mcmc.sample(50000, 0)
mcmc.trace("beta")[burn_number:].mean()
 {{< /highlight >}}
 
It is good practice to run the model several times for different starting parameters.  If the model is working well, then we would expect the different runs to converge to the same distribution.  This can be useful for catching occasions where the algorithm has converged to a state that is not the distribution we are interested in.
 
#### Autocorrelation

A big part of MCMC is that we move around the whole of parameter space, and we would like to do this as quickly as possible.  The slower we can move around, the worse convergence will be.  An indicator of this speed is the autocorrelation in samples, i.e. how close each parameter sample is to those which proceeded it.  If we have high autocorrelation, then we are moving more slowly through parameter space and convergence will be slower.

Compare the bottom left diagrams in the two charts above.  In the fist we see very low autocorrelation, while in the lower we have high autocorrelation, even at large intervals.  This suggests that the algorithm is stuck in one part of parameter space and hasn't converged to the turn distribution.

Due to the nature of MCMC we will always have autocorrelation, so we need to take steps to mitigate it.  The simplest is simply to "thin" our samples.  Rather than take every sample the algorithm takes, we take only every 5th or 10th.  This helps reduce dependence between recorded samples.

Alternatively, if the model is not very time consuming, we can mitigate by ramping up the number of samples we take.  Autocorrelation reduces the amount of information we get from each sample, but it doesn't bias the overall result, so if we have more samples we will increase the total amount of information to offset the loss.

