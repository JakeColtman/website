+++
date = "2016-05-03T08:32:19+01:00"
draft = true
description= "Why would we want to average across models?"
title = "Model Averaging - The Why"

+++

## Model Averaging - The Why

Model averaging is the process of combining many different models together to get better estimates of coefficients than could be produced by any one model.  This post will focus on the justification for this method, more details on it can be found here ((LINK HERE)).  I will start with a justification based on model theory and then provide a second based on the concept of validity hedging.

### Models as abstractions

I think the most compelling way to understand why model averaging is a good thing is to consider exactly what we're doing when we create a model. We are trying to zoom in on one relationship in the data (alternatively one causal process).  All other processes are abstracted away and bucketed into the error term.  For example, in a very simple linear regression:

<div>$$ house\_price = a + b(square\_footage) + e$$</div>

We are zooming in on how the size of a house is related to the price of the house.  All other causal factors are pushed into the error term and abstracted away.  Importantly, it is almost never the case that a model has an error term which has no causal power.  Instead, the error term contains those processes and relationships that either we choose not to model, or we cannot model due to lack of data etc.

Viewed in this light, model averaging is quite intuitive.  If we focus entirely on the best model, then we risk ignoring relationships that don't possess as much causal power, but which do capture some true relationship in the world.  If we can combine the models together, then we can get a wider and fuller picture of the world.  

A natural way to do this is to weight the contribution of each model by the probability of the model, given all of the information we have, this is exactly BMA.

### Models as validity hedging

The previous justification is somewhat theoretical, and rests on a notion of the nature of models, so I will also provide a practical justification for model averaging.  

In real world modelling, it is almost impossible to guarantee that a model we create isn't invalid due to things like missing variable bias, misspecification and so on.  Model averaging allows us to hedge against the risk that the model is wrong.  

Returning back to the previous regression by way of example, we might have another model which posits that the true relationship is:

<div>$$ house\_price = a + b(square\_footage) ^ {2} + e$$</div>

Now, neither of these models will be exactly correctly specified.  Let's assume that the true relationship lies somewhere in between.  Now, both models capture something of the true relationship, but neither does so perfectly.  By combining the two, we can get closer to the true relationship.

Again, the most obvious way to do this is through a weighted average, which is exactly BMA.

### Improving performance

Of course, the proof of these two points is in the pudding.  If we are to accept them, then it must be the case that BMA improves modelling performance.  I do not wish to assess this here, as it is wildly beyond the scope, but strong evidence of this can be found in the following papers:

Raferty, A. E., Madigan, D. and Volinsky, C. T. (1995) Accounting for Model Uncertainty in Survival Analysis Improves Predictive Performance
