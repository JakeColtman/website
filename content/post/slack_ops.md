+++
date = "2016-05-15T13:23:23+01:00"
description = "Seamless monitoring and control through Slack"
tags = []
title = "Slack-ops"
topics = []

+++

I like my architecture to be as decentalized as possible.  Systems that communicate through message passing and message queues allow components to be more easily managed, maintained and scaled.  In this sprit, the various parts of my data integration tool communicate primarily through a central messaging queue.

While this is great for the system, a messaging queue can be hard for people to interact with.  Often it requires digging into system settings (MSMQ), web interfaces (SQS) or using programming languages.  

None of these are difficult enough to be prohibitive, but they all raise barriers to access.  Making things that should be easy more difficult encourages people to avoid doing them, or coming up with work arounds.  Neither of these are good things.

Like many companies, we use Slack as our main IM tool.  Most people have it in easy access at all times through their computers, tablets or phones.  It really couldn't be much easier for people to interact with, both for reads and writes.

Leveraging Slack to make the messaging queue easier to interact with is a win-win.  The barrier to action is greatly reduced.  By using Slack as essentially a shared command line, people can seemlessly transition from discussing a solution to acting on it in just a few clicks.  Moreover, because everyone has their wn slack login, you get a built in security layer (albeit not one that you would want to solely rely on).

In terms of architecture, for small projects it is perfectly possible to abstract Slack channels into a queue.  The three main things that are needed for a messaging queue are: 

1 - The ability to post messages
2 - The ability to read messages
3 - The ability to mark messages as read

The first two requirments are trivially met by Slacks post and get_messages API functionality, no troubles at all.  Generally I wrap the get messages up into a python generator, so the end user can just poll for the next message to consume.  

The third one isn't as obvious, but we can get a neat solution by leveraging emoticons.  If we designate emoticons as having different meanings then queue consumers can mark up messages as read, errored etc.  Not massively scalable or production ready, but good enough for most scenarios where using Slack as a messaging queue is reasonable.  

For bigger projects that need to be more robust, we can still use Slack by treating it as a gateway to a proper messaging queue.  For example, set up a channel as a gateway and have a script monitoring for messages.  When it gets a new message it posts it to your messaging queue.

Going the other way, the easiest way to surface logging to slack is just to add a call to the Slack api in your logging/monitoring code.  If you wish to have more separation, you could set up another messaging queue and create a monitoring script which reads messages and pushes them to Slack.  If you are going to do this, ensure that you have some kind of breaker so that you don't end up with messages in an infinite cycle going backwards and forwards between Slack and the messaging queue!  Personally I like to have two channels, one for active commands and the other for passive logging to make this division explicit.