+++
date = "2016-05-15T13:23:23+01:00"
description = "Setting up a looker UAT environment"
tags = []
title = "Looker UAT"
topics = []

+++

Looker, the business intelligence tool, integrates with version control tools like git to allow teams to work together on building models.  The default case is for developer user to have their own branch, and for end users to see models from the main production branch.

For small projects and changes this works fine, however, the limitations of this become apparent quite quickly.  For example, changes to existing models that require UAT are very difficult to deal with.  There is also no clean way for multiple people to work on one change.  Changes are restricted to being either on a single developer's branch or on the production branch.  

Short of changes in the Looker setup to allow proper branching and full implementation of git, the best solution is to have two looker instances, one for UAT and one for production.  Developers work on the UAT version together and can use the main branch of the UAT instance to share work and present work to end users.  When the end users approve of the changes, push the main branch of UAT into a branch of the production looker instance and create a pull request.

Logistically this is quite simple to do, if you know how to set up a looker instance, then the only trick is to link the two github repos together.  To do this, we add the production looker repo as a remote in the UAT looker repo like so:

	git remote add [SHORTNAME] [URL OF PRODUCTION REPO]
	
A slight complication arises if you only have one looker instance, for example, if you only have one license.  The best solution then is to create a new project with UAT in the name.  However, because the files names of models are used as url identifiers, they need to be unique across projects.  This means that you need to prepend or append UAT onto model file names.  

In order to keep UAT and production in sync, we then need to use git hooks to rename the files from uat naming to production naming when we push to the main production repository.  Because we rarely add or remove models, I just hard coded the renaming in the git hooks like so:

	git mv uat_website.model.lookml website.model.lookml

If you use labels to override the default naming, you will need to change these too, otherwise your UAT models and production models will show up the same in the explore tab.  You can use sed to do this:

	sed -i -e 's/- label: UAT Website/- label: Website/g' website.model.lookml

Long term, this hacky solution isn't great value.  I'd strongly encourage you to get a totally sepaarte installation of looker.  Proper versioning and releases are definitely worth the additional costs.