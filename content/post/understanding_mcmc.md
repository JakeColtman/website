+++
date = "2016-05-03T19:07:58+01:00"
title = "Understanding MCMC"
description = "List of resources for learning more about MCMC"
+++

MCMC is essential to fitting Bayesian models where there aren't closed form solutions.  Without it it would not be feasible to move past simple models using conjugate priors.  While it is possible to learn to use it purely as a black box, doing so risks limiting one's understanding in the long run.  Moreover, it provides a great deal of intellectual satisfaction.

Given there are already excellent resources to learn more about MCMC, I'm not sure I can provide more value added than directing you to the following links.  The last link is particularly good if you want a deep understanding

[Bayesian methods for hackers](http://nbviewer.jupyter.org/github/CamDavidsonPilon/Probabilistic-Programming-and-Bayesian-Methods-for-Hackers/blob/master/Chapter3_MCMC/Chapter3.ipynb)

http://twiecki.github.io/blog/2015/11/10/mcmc-sampling/

Alternatively a more in-depth view at

http://www.stats.ox.ac.uk/~cholmes/Courses/BDA/bda_mcmc.pdf
